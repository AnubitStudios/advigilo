import _, { forEach } from "lodash";
import css from "../styles/style.css";
import html from "../dist/index.html";

var latestKnown_scroll_y,
  ticking = false,
  adv_count = 0,
  adv_ends = ["ANCED", "ERTISEMENT", "ICE", "ANTAGE", "IGILO"],
  rellax = new Rellax(".rellax");
AOS.init();

function changeText(e) {
  e.srcElement.innerHTML = adv_ends[adv_count];
  adv_count += 1;
  if (adv_count >= adv_ends.length) {
    adv_count = 0;
  }
}

function change(e) {
  ticking = false;
  if (10 < latestKnown_scroll_y) {
    if (!document.getElementsByTagName("h2")[0].hasAttribute("style")) {
      document.getElementsByTagName("h2")[0].style.opacity = "1";
    }
    var gradient = parseInt(100 - latestKnown_scroll_y / 14);
    if (
      gradient >= -22 &&
      document.getElementsByTagName("h2")[0].innerHTML != "Online Marketing"
    ) {
      document.getElementsByTagName("h2")[0].innerHTML = "Online Marketing";
    } else if (
      gradient < -22 &&
      gradient >= -161 &&
      document.getElementsByTagName("h2")[0].innerHTML != "Technologie"
    ) {
      document.getElementsByTagName("h2")[0].innerHTML = "Technologie";
    } else if (
      gradient < -161 &&
      gradient >= -288 &&
      document.getElementsByTagName("h2")[0].innerHTML != "Consulting"
    ) {
      document.getElementsByTagName("h2")[0].innerHTML = "Consulting";
    } else if (
      gradient < -288 &&
      document.getElementsByTagName("h2")[0].innerHTML != "Kreatives"
    ) {
      document.getElementsByTagName("h2")[0].innerHTML = "Kreatives";
    }
    document.getElementsByTagName("h2")[0].style.backgroundPosition =
      gradient + "% 100%";
    var perspective = parseInt(
      18 + (latestKnown_scroll_y * (latestKnown_scroll_y / 120)) / 1.2
    );
    var transform =
      "perspective(" +
      perspective +
      "px) rotateX(1deg) translateZ(-4px) translateY(80px)";
    document.getElementsByClassName("device")[0].style.transform = transform;
  } else if (
    latestKnown_scroll_y <= 1 &&
    document.getElementsByClassName("device")[0].hasAttribute("style") &&
    document.getElementsByTagName("h2")[0].hasAttribute("style")
  ) {
    document.getElementsByClassName("device")[0].removeAttribute("style");
    document.getElementsByTagName("h2")[0].removeAttribute("style");
    document
      .getElementsByClassName("scrolled_at")[0]
      .firstElementChild.contentDocument.getElementsByTagName("svg")[0]
      .classList.remove("shine");
    document
      .getElementsByClassName("scrolled_at")[0]
      .classList.remove("scrolled_at");
    document
      .querySelectorAll(".icons .wrapper")[0]
      .classList.add("scrolled_at");
    document
      .getElementsByClassName("scrolled_at")[0]
      .firstElementChild.contentDocument.getElementsByTagName("svg")[0]
      .classList.add("shine");
  }
  if (latestKnown_scroll_y > 200 && window.innerWidth > 880) {
    animateScroll();
    if (
      parseInt(document.getElementsByClassName("device")[0].style.top) !=
      46 - 200 / (window.innerHeight / 70)
    ) {
      var top = 46 - 200 / (window.innerHeight / 70);
      document.getElementsByClassName("device")[0].style.top = top + "vh";
    }
  } else if (latestKnown_scroll_y > 120 && window.innerWidth <= 880) {
    animateScroll();
    if (
      parseInt(document.getElementsByClassName("device")[0].style.top) !=
      46 - 120 / (window.innerHeight / 70)
    ) {
      var top = 46 - 120 / (window.innerHeight / 70);
      document.getElementsByClassName("device")[0].style.top = top + "vh";
    }
  } else if (latestKnown_scroll_y <= 200 && window.innerWidth > 880) {
    toggleSticky();
  } else if (latestKnown_scroll_y <= 120 && window.innerWidth <= 880) {
    toggleSticky();
  }
}

function animateScroll() {
  var limit = 470;
  if (
    (latestKnown_scroll_y > 200 && latestKnown_scroll_y < limit) ||
    (latestKnown_scroll_y > limit * 4 && latestKnown_scroll_y < limit * 5) ||
    (latestKnown_scroll_y > limit * 8 && latestKnown_scroll_y < limit * 9) ||
    (latestKnown_scroll_y > limit * 12 && latestKnown_scroll_y < limit * 13)
  ) {
    document
      .getElementsByClassName("scrolled_at")[0]
      .firstElementChild.contentDocument.getElementsByTagName("svg")[0]
      .classList.remove("shine");
    document
      .getElementsByClassName("scrolled_at")[0]
      .classList.remove("scrolled_at");
    document
      .querySelectorAll(".icons .wrapper")[0]
      .classList.add("scrolled_at");
    document
      .getElementsByClassName("scrolled_at")[0]
      .firstElementChild.contentDocument.getElementsByTagName("svg")[0]
      .classList.add("shine");
  } else if (
    (latestKnown_scroll_y > limit && latestKnown_scroll_y < limit * 2) ||
    (latestKnown_scroll_y > limit * 5 && latestKnown_scroll_y < limit * 6) ||
    (latestKnown_scroll_y > limit * 9 && latestKnown_scroll_y < limit * 10) ||
    (latestKnown_scroll_y > limit * 13 && latestKnown_scroll_y < limit * 14)
  ) {
    document
      .getElementsByClassName("scrolled_at")[0]
      .firstElementChild.contentDocument.getElementsByTagName("svg")[0]
      .classList.remove("shine");
    document
      .getElementsByClassName("scrolled_at")[0]
      .classList.remove("scrolled_at");
    document
      .querySelectorAll(".icons .wrapper")[1]
      .classList.add("scrolled_at");
    document
      .getElementsByClassName("scrolled_at")[0]
      .firstElementChild.contentDocument.getElementsByTagName("svg")[0]
      .classList.add("shine");
  } else if (
    (latestKnown_scroll_y > limit * 2 && latestKnown_scroll_y < limit * 3) ||
    (latestKnown_scroll_y > limit * 6 && latestKnown_scroll_y < limit * 7) ||
    (latestKnown_scroll_y > limit * 10 && latestKnown_scroll_y < limit * 11) ||
    (latestKnown_scroll_y > limit * 14 && latestKnown_scroll_y < limit * 15)
  ) {
    document
      .getElementsByClassName("scrolled_at")[0]
      .firstElementChild.contentDocument.getElementsByTagName("svg")[0]
      .classList.remove("shine");
    document
      .getElementsByClassName("scrolled_at")[0]
      .classList.remove("scrolled_at");
    document
      .querySelectorAll(".icons .wrapper")[2]
      .classList.add("scrolled_at");
    document
      .getElementsByClassName("scrolled_at")[0]
      .firstElementChild.contentDocument.getElementsByTagName("svg")[0]
      .classList.add("shine");
  } else if (
    (latestKnown_scroll_y > limit * 3 && latestKnown_scroll_y < limit * 4) ||
    (latestKnown_scroll_y > limit * 7 && latestKnown_scroll_y < limit * 8) ||
    (latestKnown_scroll_y > limit * 11 && latestKnown_scroll_y < limit * 12) ||
    (latestKnown_scroll_y > limit * 15 && latestKnown_scroll_y < limit * 16)
  ) {
    document
      .getElementsByClassName("scrolled_at")[0]
      .firstElementChild.contentDocument.getElementsByTagName("svg")[0]
      .classList.remove("shine");
    document
      .getElementsByClassName("scrolled_at")[0]
      .classList.remove("scrolled_at");
    document
      .querySelectorAll(".icons .wrapper")[3]
      .classList.add("scrolled_at");
    document
      .getElementsByClassName("scrolled_at")[0]
      .firstElementChild.contentDocument.getElementsByTagName("svg")[0]
      .classList.add("shine");
  }
  if (
    !document.getElementsByClassName("sticky")[0].classList.contains("unsticky")
  ) {
    document.getElementsByClassName("sticky")[0].classList.add("unsticky");
    if (
      document
        .getElementsByClassName("dashboard")[0]
        .contentDocument.getElementById("dash_svg") != null
    ) {
      document
        .getElementsByClassName("dashboard")[0]
        .contentDocument.getElementById("dash_svg")
        .classList.add("data_visuals");
    }
  }
}

function toggleSticky() {
  var top = 46 - latestKnown_scroll_y / (window.innerHeight / 70);
  document.getElementsByClassName("device")[0].style.top = top + "vh";
  if (
    document.getElementsByClassName("sticky")[0].classList.contains("unsticky")
  ) {
    document.getElementsByClassName("sticky")[0].classList.remove("unsticky");
    document
      .getElementsByClassName("dashboard")[0]
      .contentDocument.getElementById("dash_svg")
      .classList.remove("data_visuals");
  }
}

function onScroll() {
  latestKnown_scroll_y = window.scrollY;
  requestTick();
}

function requestTick() {
  if (!ticking) {
    requestAnimationFrame(change);
  }
  ticking = true;
}

function enableResponsiveScrolling() {
  var perspective = parseInt(
    18 + (window.scrollY * (window.scrollY / 120)) / 1.2
  );
  var transform =
    "perspective(" +
    perspective +
    "px) rotateX(1deg) translateZ(-4px) translateY(80px)";
  document.getElementsByClassName("device")[0].style.transform = transform;
  if (perspective > 300 && window.innerWidth > 880) {
    var top = 46 - 200 / (window.innerHeight / 70);
    document.getElementsByClassName("device")[0].style.top = top + "vh";
  } else if (perspective > 300 && window.innerWidth <= 880) {
    var top = 46 - 120 / (window.innerHeight / 70);
    document.getElementsByClassName("device")[0].style.top = top + "vh";
  } else if (perspective <= 300) {
    var top = 46 - window.scrollY / (window.innerHeight / 70);
    document.getElementsByClassName("device")[0].style.top = top + "vh";
  }
}

function onLoad() {
  document
    .getElementsByClassName("variable_text")[0]
    .addEventListener("animationiteration", changeText);
  setTimeout(() => {
    enableResponsiveScrolling();
  }, 100);
}
window.addEventListener("scroll", onScroll);
window.addEventListener("resize", enableResponsiveScrolling);
window.addEventListener("load", onLoad);
